// Nomor 1
function arrayToObject(anArray) {
  if (anArray.length == 0) {
    console.log('""');
  } else {
    var now = new Date();
    var thisYear = now.getFullYear();

    var listPerson = [];

    for (var i = 0; i < anArray.length; i++) {
      var theObject = {};
      theObject["firstName"] = anArray[i][0];
      theObject["lastName"] = anArray[i][1];
      theObject["gender"] = anArray[i][2];
      if (anArray[i][3] > thisYear || anArray[i][3] == undefined) {
        theObject["age"] = "Invalid birth year";
      } else {
        theObject["age"] = thisYear - anArray[i][3];
      }

      listPerson[i] = theObject;
    }

    for (var i = 0; i < listPerson.length; i++) {
      console.log(
        i +
          1 +
          ". " +
          listPerson[i].firstName +
          " " +
          listPerson[i].lastName +
          ": " +
          JSON.stringify(listPerson[i])
      );
    }
  }
}

// Driver Code
var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case
arrayToObject([]); // ""

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

// Nomor 2
function shoppingTime(memberId, money) {
  var JualanToko = {
    "Sepatu Stacattu": 1500000,
    "Baju Zoro": 500000,
    "Baju H&N": 250000,
    "Sweater Uniklooh": 175000,
    "Casing Handphone": 50000,
  };

  if (memberId == "" || memberId == undefined) {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  } else if (money < 50000) {
    return "Mohon maaf, uang tidak cukup";
  } else {
    var theObject = {};
    var shopList = [];
    var sisaUang = money;
    var incremnt = 0;
    while (true) {
      if (sisaUang >= JualanToko[Object.keys(JualanToko)[incremnt]]) {
        shopList.push(Object.keys(JualanToko)[incremnt]);
        sisaUang -= JualanToko[Object.keys(JualanToko)[incremnt]];
        incremnt++;
      } else if (incremnt == 5) {
        break;
      } else {
        incremnt++;
      }
    }
    theObject["memberId"] = memberId;
    theObject["money"] = money;
    theObject["listPurchased"] = shopList;
    theObject["changeMoney"] = sisaUang;
    return theObject;
  }
}

// TEST CASES
console.log(shoppingTime("1820RzKrnWn08", 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime("82Ku8Ma742", 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime("", 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime("234JdhweRxa53", 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ///Mohon maaf, toko X hanya berlaku untuk member saja

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

// Nomor 3
function naikAngkot(arrPenumpang) {
  var rute = ["A", "B", "C", "D", "E", "F"];
  var listPenumpang = [];

  for (var i = 0; i < arrPenumpang.length; i++) {
    var theObject = {};
    var banyakRute = 0;
    var awal = 0;
    var akhir = 0;
    theObject["penumpang"] = arrPenumpang[i][0];
    theObject["naikDari"] = arrPenumpang[i][1];
    theObject["tujuan"] = arrPenumpang[i][2];
    switch (arrPenumpang[i][1]) {
      case "A":
        awal = 1;
        break;
      case "B":
        awal = 2;
        break;
      case "C":
        awal = 3;
        break;
      case "D":
        awal = 4;
        break;
      case "E":
        awal = 5;
        break;
      case "F":
        awal = 6;
        break;
    }
    switch (arrPenumpang[i][2]) {
      case "A":
        akhir = 1;
        break;
      case "B":
        akhir = 2;
        break;
      case "C":
        akhir = 3;
        break;
      case "D":
        akhir = 4;
        break;
      case "E":
        akhir = 5;
        break;
      case "F":
        akhir = 6;
        break;
    }
    banyakRute = awal - akhir;
    theObject["bayar"] = Math.abs(2000 * banyakRute);
    listPenumpang.push(theObject);
  }
  return listPenumpang;
}

//TEST CASE
console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor
