// Nomor 1 Release 0
class Animal {
  constructor(name, legs = 4, cold_blooded = false) {
    this.__name = name;
    this.__legs = legs;
    this.__cold_blooded = cold_blooded;
  }
  get name() {
    return this.__name;
  }
  get legs() {
    return this.__legs;
  }
  get cold_blooded() {
    return this.__cold_blooded;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

// Nomor 1 Release 1
class Ape extends Animal {
  constructor(name, legs = 2, cold_blooded) {
    super(name, legs, cold_blooded);
  }
  yell() {
    console.log("Auooo");
  }
}

class Frog extends Animal {
  constructor(name, legs, cold_blooded) {
    super(name, legs, cold_blooded);
  }
  jump() {
    console.log("hop hop");
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

// Nomor 2
class Clock {
  constructor(template) {
    this.__template = template.template;
    this.__timer;
  }

  render() {
    this.__date = new Date();
    this.__hours = this.__date.getHours();
    this.__mins = this.__date.getMinutes();
    this.__secs = this.__date.getSeconds();

    if (this.__hours < 10) this.__hours = "0" + this.__hours;
    if (this.__mins < 10) this.__mins = "0" + this.__mins;
    if (this.__secs < 10) this.__secs = "0" + this.__secs;

    this.__output = this.__template
      .replace("h", this.__hours)
      .replace("m", this.__mins)
      .replace("s", this.__secs);

    console.log(this.__output);
  }

  stop() {
    clearInterval(this.__timer);
  }

  start() {
    this.render();
    this.__timer = setInterval(this.render.bind(this), 1000);
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();
