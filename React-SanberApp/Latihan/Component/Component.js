import React, { Component } from "react";
import { View, Text, TextInput } from "react-native";

export default class App extends Component {
  constructor() {
    super();
    this.qtate = {
      value: "Useless Placeholder",
    };
  }

  render() {
    return (
      <View>
        <TextInput
          style={{ height: 40, borderColor: "gray", borderWidth: 1 }}
          onChangeText={(text) => this.setState({ value: text })}
          value={this.qtate.value}
        />
        <Text>{this.qtate.value}</Text>
      </View>
    );
  }
}
