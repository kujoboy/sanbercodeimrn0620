import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View, ScrollView } from "react-native";

// import Component from "./Tugas/Tugas12/App";
// import Materi3 from "./Materi3_3/index";
import Login from "./Tugas/Tugas13/LoginScreen";
import Register from "./Tugas/Tugas13/RegisterScreen";
import About from "./Tugas/Tugas13/AboutScreen";

export default function App() {
  return (
    // <View style={styles.container}>
    //   <Text>Open up App.js to start working on your app!</Text>
    //   <StatusBar style="auto" />
    // </View>
    // <View></View> //Kosong
    // <Component />
    // <Materi3 />
    <ScrollView>
      <Login />

      <Register />

      <About />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
