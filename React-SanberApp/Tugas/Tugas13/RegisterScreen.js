import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  TextInput,
  ScrollView,
  Button,
  Alert,
} from "react-native";

export default class RegisterScreen extends Component {
  render() {
    return (
      <View style={style.container}>
        <Image
          source={require("./images/logo.png")}
          style={{ width: 300, height: 80 }}
        ></Image>
        <Text style={style.fieldCategory}>Register</Text>
        <View style={style.field}>
          <Text>Username</Text>
          <TextInput
            style={style.fieldBox}
            onChangeText={(text) => this.setState({ text })}
          />
        </View>
        <View style={style.field}>
          <Text>Email</Text>
          <TextInput
            style={style.fieldBox}
            onChangeText={(text) => this.setState({ text })}
          />
        </View>
        <View style={style.field}>
          <Text>Password</Text>
          <TextInput
            style={style.fieldBox}
            onChangeText={(text) => this.setState({ text })}
          />
        </View>
        <View style={style.field}>
          <Text>Ulangi Password</Text>
          <TextInput
            style={style.fieldBox}
            onChangeText={(text) => this.setState({ text })}
          />
        </View>
        <View style={style.buttonBottom}>
          <TouchableOpacity style={style.buttonSecond}>
            <Text style={style.buttonText}>Daftar</Text>
          </TouchableOpacity>
          <Text style={{ marginBottom: 20, marginTop: 20 }}>atau</Text>
          <TouchableOpacity style={style.buttonFirst}>
            <Text style={style.buttonText}>Masuk ?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 50,
  },
  fieldCategory: {
    textTransform: "uppercase",
    textAlign: "center",
    fontSize: 18,
    marginTop: 40,
    marginBottom: 40,
  },
  field: {
    alignSelf: "flex-start",
  },
  fieldBox: {
    width: 280,
    borderWidth: 1,
    marginBottom: 20,
  },
  buttonBottom: {
    flexDirection: "column",
    alignItems: "center",
  },
  buttonFirst: {
    width: 150,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: "#003366",
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#fff",
  },
  buttonSecond: {
    width: 150,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: "#3EC6FF",
    borderRadius: 25,
    borderWidth: 1,
    borderColor: "#fff",
  },
  buttonText: {
    color: "white",
    textAlign: "center",
  },
});
