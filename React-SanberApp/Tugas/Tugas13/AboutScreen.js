import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  TouchableOpacity,
  Text,
  FlatList,
  TextInput,
  ScrollView,
  Button,
  Alert,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { AntDesign } from "@expo/vector-icons";

export default class AboutScreen extends Component {
  render() {
    return (
      <View style={style.container}>
        <View style={style.head}>
          <Text style={style.headTitle}>Tentang Saya</Text>
          <TouchableOpacity>
            <Icon name="account-circle" size={150} color="#EFEFEF" />
          </TouchableOpacity>
          <Text style={style.headName}>Alsiendo D.</Text>
          <Text style={style.headJob}>React Native Developer</Text>
        </View>
        <View style={style.box}>
          <Text>Portfolio</Text>
          <View style={style.portfolioItems}>
            <View style={style.portfolioAnItem}>
              <AntDesign name="gitlab" size={24} color="#3EC6FF" />
              <Text>@kujoboy</Text>
            </View>
            <View style={style.portfolioAnItem}>
              <AntDesign name="github" size={24} color="#3EC6FF" />
              <Text>@kujoboy</Text>
            </View>
          </View>
        </View>
        <View style={style.box}>
          <Text>Hubungi Saya</Text>
          <View style={style.contactItems}>
            <AntDesign name="facebook-square" size={24} color="#3EC6FF" />
            <Text>Alsiendo Dewantara</Text>
          </View>
          <View style={style.contactItems}>
            <AntDesign name="instagram" size={24} color="#3EC6FF" />
            <Text>en_siendo</Text>
          </View>
          <View style={style.contactItems}>
            <AntDesign name="twitter" size={24} color="#3EC6FF" />
            <Text>@alsiendod</Text>
          </View>
        </View>
      </View>
    );
  }
}

const style = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    paddingTop: 50,
  },
  head: {
    flexDirection: "column",
    alignItems: "center",
  },
  headTitle: {
    fontSize: 24,
    color: "#003366",
    fontWeight: "bold",
  },
  headName: {
    fontSize: 18,
    color: "#003366",
    fontWeight: "bold",
  },
  headJob: {
    color: "#3EC6FF",
  },
  portfolioItems: {
    marginTop: 20,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  box: {
    marginTop: 20,
    backgroundColor: "#EFEFEF",
    borderRadius: 25,
    padding: 15,
  },
  portfolioAnItem: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  contactItems: {
    marginTop: 15,
    flexDirection: "row",
    marginLeft: 55,
  },
});
