import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image,
} from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";
import { AntDesign } from "@expo/vector-icons";

export default class Main extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View>
          <Image
            source={require("./images/logo.png")}
            style={{ width: 150, height: 40 }}
          ></Image>
        </View>
        <View>
          <Icon name="account-circle" size={40} color="#EFEFEF" />
          <Text>Hai,</Text>
          <Text>Alsiendo D.</Text>
        </View>
        <View>
          <Text>SKILL</Text>
          <View></View>
          <View>
            <TouchableOpacity>
              <Text>Library/Framework</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text>Bahasa Pemrograman</Text>
            </TouchableOpacity>
            <TouchableOpacity>
              <Text>Teknologi</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
