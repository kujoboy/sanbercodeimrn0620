//! Nomor 1
const golden = () => "this is golden!!";
console.log(golden());

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

//! Nomor 2
const newFunction = (firstName, lastName) => ({
  firstName: firstName,
  lastName: lastName,
  fullName: () => console.log(firstName + " " + lastName),
});

newFunction("William", "Imoh").fullName();

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

//! Nomor 3
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!",
};

const { firstName, lastName, destination, occupation } = newObject;
console.log(firstName, lastName, destination, occupation);

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

//! Nomor 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
const combined = [...west, ...east];

console.log(combined);

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

//! Nomor 5
const planet = "earth";
const view = "glass";

const before = `Lorem ${view} dolor sit amet, \
consectetur adipiscing elit, \
${planet} do eiusmod tempor incididunt \
ut labore et dolore magna aliqua. \
Ut enim ad minim veniam`;

console.log(before);

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor
