// di index.js
var readBooks = require("./callback.js");
var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var count = 0;
function loopingfunction(time) {
  if (time >= 2000) {
    readBooks(time, books[count], (sisaWaktu) => loopingfunction(sisaWaktu));
  }
  count++;
}

loopingfunction(10000);
