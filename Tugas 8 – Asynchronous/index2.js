var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

var count = 0;
function loopingfunction(time) {
  if (time >= 2000) {
    readBooksPromise(time, books[count])
      .then((hasil) => loopingfunction(hasil))
      .catch((error) => console.log(error));
  }
  count++;
}

loopingfunction(10000);
