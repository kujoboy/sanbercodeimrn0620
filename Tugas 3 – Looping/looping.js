// Nomor 1
var count = 0;
console.log("LOOPING PERTAMA");
while (count < 20) {
  count += 2;
  console.log(count + " - I love coding");
}
console.log("LOOPING KEDUA");
while (count > 0) {
  console.log(count + " - I love coding");
  count -= 2;
}

console.log(""); //Baris pemisah antar nomor

// Nomor 2
for (var angka = 1; angka <= 20; angka++) {
  if (angka % 2 != 0) {
    if (angka % 3 == 0) {
      console.log(angka + " - I Love Coding");
    } else {
      console.log(angka + " - Santai");
    }
  } else if (angka % 2 == 0) {
    console.log(angka + " - Berkualitas");
  }
}

console.log(""); //Baris pemisah antar nomor

// Nomor 3
var oneline = "";
for (var i = 0; i < 4; i++) {
  for (var j = 0; j < 8; j++) {
    oneline += "#";
  }
  console.log(oneline);
  oneline = "";
}

console.log(""); //Baris pemisah antar nomor

// Nomor 4
oneline = "";
for (var i = 0; i < 7; i++) {
  for (var j = 0; j <= i; j++) {
    oneline += "#";
  }
  console.log(oneline);
  oneline = "";
}

console.log(""); //Baris pemisah antar nomor

// Nomor 5
var check = false;
oneline = "";
for (var i = 0; i < 8; i++) {
  for (var j = 0; j < 8; j++) {
    if (check) {
      oneline += "#";
      check = false;
    } else {
      oneline += " ";
      check = true;
    }
  }
  console.log(oneline);
  oneline = "";
  if (check) {
    check = false;
  } else {
    check = true;
  }
}
