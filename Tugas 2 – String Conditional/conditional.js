// If-else
var nama = "Siendo";
var peran = "Guard";

if (nama == "") {
  console.log("Nama harus diisi!");
} else {
  if (peran == "") {
    console.log("Halo " + nama + ", Pilih Peranmu untuk memulai game.");
  } else {
    console.log("Selamat datang di dunia Werewolf, " + nama);
    if (peran == "Penyihir") {
      console.log(
        "Halo " +
          peran +
          " " +
          nama +
          ", kamu dapat melihat siapa yang menjadi werewolf!"
      );
    } else if (peran == "Guard") {
      console.log(
        "Halo " +
          peran +
          " " +
          nama +
          ", kamu akan membantu melindungi temanmu dari serangan werewolf."
      );
    } else if (peran == "Werewolf") {
      console.log(
        "Halo " +
          peran +
          " " +
          nama +
          ", Kamu akan memakan mangsa setiap malam!"
      );
    }
  }
}

console.log(""); //Baris pemisah antar sub bab

var hari = 8;
var bulan = 6;
var tahun = 2000;

var stringresult = hari + " ";

switch (bulan) {
  case 1: {
    stringresult += " Januari " + tahun;
    break;
  }
  case 2: {
    stringresult += " Februari " + tahun;
    break;
  }
  case 3: {
    stringresult += " Maret " + tahun;
    break;
  }
  case 4: {
    stringresult += " April " + tahun;
    break;
  }
  case 5: {
    stringresult += " Mei " + tahun;
    break;
  }
  case 6: {
    stringresult += " Juni " + tahun;
    break;
  }
  case 7: {
    stringresult += " Juli " + tahun;
    break;
  }
  case 8: {
    stringresult += " Agustus " + tahun;
    break;
  }
  case 9: {
    stringresult += " September " + tahun;
    break;
  }
  case 10: {
    stringresult += " Oktober " + tahun;
    break;
  }
  case 11: {
    stringresult += " November " + tahun;
    break;
  }
  case 12: {
    stringresult += "Desember " + tahun;
    break;
  }
  default: {
    stringresult = "Maaf, bulan hari diantara 1 - 12";
    break;
  }
}

console.log(stringresult);
