// Nomor 1
function range(startNum, finishNum) {
  if (finishNum == undefined) {
    return -1;
  } else {
    var array1 = [];
    if (startNum < finishNum) {
      for (var i = startNum; i <= finishNum; i++) {
        array1.push(i);
      }
    } else {
      for (var i = startNum; i >= finishNum; i--) {
        array1.push(i);
      }
    }
    return array1;
  }
}

console.log(range(1, 10));
console.log(range(1));
console.log(range(11, 18));
console.log(range(54, 50));
console.log(range());

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

// Nomor 2
function rangeWithStep(startNum, finishNum, step) {
  if (finishNum == undefined) {
    return -1;
  } else {
    var array1 = [];
    if (startNum < finishNum) {
      for (var i = startNum; i <= finishNum; i += step) {
        array1.push(i);
      }
    } else {
      for (var i = startNum; i >= finishNum; i -= step) {
        array1.push(i);
      }
    }
    return array1;
  }
}

console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

// Nomor 3
function sum(startNum = 0, finishNum = 0, step = 1) {
  var result = 0;
  if (finishNum == 0) {
    result = startNum;
  } else {
    if (startNum > finishNum) {
      [startNum, finishNum] = [finishNum, startNum];
    }
    for (var i = startNum; i <= finishNum; i += step) {
      result += i;
    }
  }
  return result;
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

// Nomor 4
function dataHandling(input) {
  for (var i = 0; i < input.length; i++) {
    console.log("Nomor ID:  " + input[i][0]);
    console.log("Nama Lengkap:  " + input[i][1]);
    console.log("TTL: " + input[i][2] + " " + input[i][3]);
    console.log("Hobi: " + input[i][4]);
    console.log();
  }
}

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"],
];

dataHandling(input);

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

// Nomor 5
// alternative penyelesaian menggunakan built-in method
function balikKata_alternative(aString) {
  var collapse = aString.split("").reverse().join("");
  return collapse;
}

function balikKata(aString) {
  var final_result = "";
  for (var i = aString.length - 1; i >= 0; i--) {
    final_result += aString[i];
  }
  return final_result;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));

console.log(""); //Baris pemisah antar nomor
console.log(""); //Baris pemisah antar nomor

// Nomor 6
function dataHandling2(array) {
  var first_result = array.slice();
  var second_result = "";
  var third_result = "";

  first_result.splice(1, 1);
  first_result.splice(1, 0, "Roman Alamsyah Elsharawy");
  first_result.splice(2, 1);
  first_result.splice(2, 0, "Provinsi Bandar Lampung");
  first_result.splice(4, 1);
  first_result.splice(4, 0, "Pria");
  first_result.splice(5, 1);
  first_result.splice(5, 0, "SMA Internasional Metro");
  console.log(first_result);

  var theDate = array[3].split("/");
  var month = theDate[1];
  switch (month) {
    case "01":
      second_result += "Januari";
      break;

    case "02":
      second_result += "Februari";
      break;

    case "03":
      second_result += "Maret";
      break;

    case "04":
      second_result += "April";
      break;

    case "05":
      second_result += "Mei";
      break;

    case "06":
      second_result += "Juni";
      break;

    case "07":
      second_result += "Juli";
      break;

    case "08":
      second_result += "Agustus";
      break;

    case "09":
      second_result += "September";
      break;

    case "10":
      second_result += "Oktober";
      break;

    case "11":
      second_result += "November";
      break;

    case "12":
      second_result += "Desember";
      break;
  }
  console.log(second_result);

  var sortDate = theDate.slice();
  sortDate.sort(function (a, b) {
    return b - a;
  });
  console.log(sortDate);

  var joinDate = theDate.join("-");
  console.log(joinDate);

  var name = array[1];
  console.log(name.slice(0, 15));
}

var input = [
  "0001",
  "Roman Alamsyah ",
  "Bandar Lampung",
  "21/05/1989",
  "Membaca",
];
dataHandling2(input);
